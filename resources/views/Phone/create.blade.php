

{!! Form::open(array('method'=>'POST','route'=>array('phone.store'))) !!}

<div class="form-group">
    {!! Form::label('Your Name') !!}
    {!! Form::text('name', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your name')) !!}
</div>
<br><br>
<div class="form-group">
    {!! Form::label('Your Number') !!}
    {!! Form::number('number', null, 
        array('required', 
              'class'=>'form-control', 
              'placeholder'=>'Your e-mail address')) !!}
</div>

<br><br>

<div class="form-group">
    {!! Form::submit('Save!', 
      array('class'=>'btn btn-primary')) !!}
</div>
{!! Form::close() !!}