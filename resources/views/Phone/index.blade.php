@if(Session::has('flash_message'))
    <h2>{{ Session::get('flash_message')}}</h2>
@endif
<a href="{!! url('phone/create') !!}">Add New</a>
<h1>Phone Numbers</h1>
<table border="1">
    <tr>
    <td>SL NO</td>
        <td>Name</td>
         <td>Number</td>

        <td>Action</td>
    </tr>
        <?php $i = 0 ?>
    @foreach($phones as $phone)
     <?php $i++ ?>
    <tr>
      <td>{{ $i}}</td>
        <td>{!! $phone->name !!}</td>
         <td>{!! $phone->number !!}</td>
        <td>
            <a href="{!! url('phone/'.$phone->id.'/edit') !!}">Edit</a> |
            <a href="{!! url('phone/'.$phone->id) !!}">View</a> |
            {!! Form::open(array('method'=>'DELETE','route'=>array('phone.destroy',$phone->id))) !!}
            {!! Form::submit('delete') !!}
            {!! Form::close() !!}
            
        </td>
    </tr>
    @endforeach
</table>